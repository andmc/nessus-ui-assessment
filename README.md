# Tenable - Nessus UI Assessment
Tenable test problems description are below. Below them will be instructions to see the solution of each one.

> 1. A server sends the following data when requesting 'http://[yourserver]/download/request?host=2':

```javascript
{
    "configurations": [
        {
            "name": "host1",
            "hostname": "nessus-ntp.lab.com",
            "port": 1241,
            "username": "Toto"
        },
        {
            "name": "host2",
            "hostname": "nessus-xml.lab.com",
            "port": 3384,
            "username": "admin"
        }
    ]
}
```
> Write an HTML/JS code to send this request and display the result.

> 2.  Use jQuery to add some style and design to the previous code.
> 3.  Add some CSS design/style
> 4.  Use all the javascript libraries you want and/or like and show us your skills on this small web application.
> 5.  Instead of sending host=2 you now send host=10000 and the response contains 10000 fields. How would you handle that? What could be improved to get the best performance?

----
## Overview:
There is one branch for each of the solution for the problems 1, 2, 3, 4 and 5. **The answer for problem 5 is on the README file on the branch problem-5**.

The porpuse of this test is to show Front End skills, but in order to make the application more realistic I set up a webserver (express) with a few endpoints to make the http request described in *problem 1*, if interest to see that code the path is: buildScripts/srcServer.js

Because there wasn't any constraint on which version of javascript to use I decided to choose ES6, I'm using ES6 modules, which is why I used webpack to bundle up the modules and Babel to transpile to ES5.

*   Clone this repo ```$ git clone ```
*   Install node
*   Make sure you are on the root of the repository
*   run ```$ npm install``` and wait for dependencies to be installed.
*   run ```$ npm start -s``` to launch the application on your default browser.
*   The Branches for each problem are:
    *   Problem 1: *problem-1*; run ```$ git checkout problem-1```
    *   Problem 2: *problem-2*; run ```$ git checkout problem-2```
    *   Problem 3: *problem-3*; run ```$ git checkout problem-3```
    *   Problem 4: *problem-4*; run ```$ git checkout problem-4```
    *   Problem 5: *problem-5*; run ```$ git checkout problem-5```


## Application root
This structure will be the same for all the problems, what changes is the code on the index.js, userApi.js and index.css for almost each problem.

```javascript
src/
    api/
        baseUrl.js  //return the domain name.
        userApi.js  //this file contains the asynchronous http request.
    index.html  // contains the html to show the results.
    index.css   // some basic styles
    index.js    // contains the code to render the results of the http request into the DOM
```
On all the problems, the http response is cached, so the first time the page loads it will make the http request with ajax, then if the page is refreshed the data will be loaded from cache. So when switching between branches and you are inspecting the web app with developer tools, it will be good to clear localstorage everytime you want to see the ajax request. This manually clearing the cache from localstorage could be avoided if the server caches the request, usually in order to not doing a query on the database. if the content of the response rearly changes then localstorage will be a good solution, and also it could be used besides the webserver cache, but it will require more logic to know how and when to delete or update the data.


For problem 4, I only used Fetch and jQuery, what matters for me is using the proper tools and know it's implementations rather than using a tool because everyone is using it, that said, I don't think the use of an MVC framework or library is necessary for this small web app. Probably in problem 5 will be good to use one of them, but also not necessary.
