// configure the webserver to serve up the files on the src directory

import express from 'express';
import path from 'path';
import open from 'open';
import webpack from 'webpack';
import config from '../webpack.config.dev';

/* eslint-disable no-console */

const port = 3000;
const app = express();
const compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '../src/index.html'));
});

app.get('/index.css', function(req, res) {
    res.sendFile(path.join(__dirname, '../src/index.css'));
});

app.get('/download/request', function(req, res) {
    var host = req.query.host;
    if ( host == '2' ) {
        res.json(
            {
                "configurations": [
                    {"name": "host1", "hostname":"nessus-ntp.lab.com", "port":1241,"username":"toto"},
                    {"name": "host2", "hostname":"nessus-xml.lab.com", "port":3384,"username":"admin"}
                ]
            }
        );
    }
});

app.listen(port, function(err) {
    if (err) {
        console.log(err);
    } else {
        open('http://localhost:' + port);
    }
});
