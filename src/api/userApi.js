import 'whatwg-fetch';
import getBaseUrl from './baseUrl';

const baseUrl = getBaseUrl();

var localStorage = window.localStorage;

export function getUsers() {
    return get('download/request?host=2');
}

function get(url) {
    var data;

    /**
     * check if http response is cached
     * if it is cached asign it to @data
     */
    if (!localStorage.getItem('configurations')) {
        localStorage.setItem('configurations', '');
    } else {
        data = localStorage.getItem('configurations');
    }

    /**
     * if the response is cached
     * return the value based on a Promise object to make the result allways async.
     * if not, use fetch api, to make an asynchronous request.
     */
    if(!data) {
        //The fetch() function is a Promise-based mechanism for programmatically making web requests in the browser
        var prom = fetch(baseUrl + url).then(onSuccess, onError)

        prom.then(function(value) {
            localStorage.setItem('configurations', JSON.stringify(value));
        });

        return prom;
    } else {
        return Promise.resolve(JSON.parse(data));
    }
}

function onSuccess(response) {
    return response.json();
}

function onError(error) {
    console.log(error);     // eslint-disable-line no-console
}

