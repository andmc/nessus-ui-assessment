import $ from 'jquery';
import {getUsers} from './api/userApi';

getUsers().then(result => {
    let usersBody = "";

    var configurations = result.configurations;

    configurations.forEach(user => {
        usersBody += `<tr>
            <td>${user.name}</td>
            <td>${user.hostname}</td>
            <td>${user.port}</td>
            <td>${user.username}</td>
        </tr>`;
    });

    $('#users').html(usersBody);

});
